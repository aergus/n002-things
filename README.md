# n002-things.sty

This repository contains `n002-things.sty`, a LaTeX style file which requires
some useful packages and provides some useful macros.

It in particular lays ground for [`n002-math.sty`][n002-math], a style file
with math-related stuff (and therefore doesn't contain anything math-related).


[n002-math]: https://gitlab.com/aergus/n002-math

